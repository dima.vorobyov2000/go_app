FROM golang:1.18

WORKDIR /go_app

ADD . /go_app/

RUN go get github.com/go-sql-driver/mysql
RUN go get golang.org/x/crypto/bcrypt
EXPOSE 9090

RUN ["go", "build", "-o", "./golang", "main.go"]
CMD [ "./golang" ]